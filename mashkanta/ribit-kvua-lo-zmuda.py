#!usr/bin/python3

import argparse
from numpy import random
import numpy as np
import fileinput
import sys
import select
import fcntl
import os
import pandas as pd

# calcualte according to wiki: https://en.wikipedia.org/wiki/Mortgage_calculator#Monthly_payment_formula

#rate = yearly rate in percentage
#years = the number of years to spand the loan
#principal = the size of the loan

def ribit_kvua_lo_zmuda_monthly_return(principal, rate, years, months = 0):
    months = years * 12 if months == 0 else months
    mrate = rate/1200 #note, the monthly rate is calcuated here assuming ribit nominalit //todo -understand differnces
    m = pow(1 + mrate, months)
    c = principal * (mrate * m) / (m - 1)
    return c

#return a np arr of monthly returns based on prediction of the
#rate = yearly rate in percentage
#years = the number of years to spand the loan
#principal = the size of the loan
#madad: yearly madad rate in percentage. note the case of 'madad == 0' is
#       equivalent to having 'ribit kvua lo zmuda'
def ribit_kvua_zmuda_monthly_return(principal, years, rate, madad, is_spizer):
    months = years * 12
    mrate = rate/1200
    mmadad = madad/1200
    p = principal
    c = 0;
    mn = months
    hechzer_kolel_arr = []
    hechzer_haribit_arr = []
    hechzer_hakeren_arr = []
    keren_shava_base = principal / months
    for i in range(months):
        p = (1 + mmadad) * p
        if is_spizer:
            c = ribit_kvua_lo_zmuda_monthly_return(p, rate, 0, mn)
        else:
            # in 'keren shava' we pay each month the original principal per month
            # (that is keren_shava_base) and the rate left from the current principal's "itra"
            # (which is 'mrate * p'
            c = keren_shava_base + mrate * p
        hechzer_kolel_arr.append(c)
        if is_spizer:
            # in spizer we pay each moth, the ribit from current principal (p *mrate)
            # and the reset (c - (p * mrate) goes to pay back the keren.
            hechzer_haribit_arr.append(p * mrate)
            hechzer_hakeren_arr.append(c - (p * mrate))
        else:
            hechzer_hakeren_arr.append(keren_shava_base)
            hechzer_haribit_arr.append(c - keren_shava_base)

        mn = mn - 1
        p = p - (c - (p * mrate))
    data = {'pay for keren' : hechzer_hakeren_arr, 'pay for ribit' : hechzer_haribit_arr, 'pay kolel' : hechzer_kolel_arr}
    df = pd.DataFrame(data)
    print(df)
    return hechzer_kolel_arr[0]

parser = argparse.ArgumentParser("good luck")

parser.add_argument("-p", "--principal",type=int, help="the size of the loan", required=True)
parser.add_argument("-y", "--years",type=int, help="the number of years to spand the loan", required=True)
parser.add_argument("-r", "--rate", type=float, help="yearly rate in percentage (standart nominative ribit)", required=True)
parser.add_argument("-m", "--madad", type=float, default = 0, help="yearly rate in percentage")
parser.add_argument("-k", "--keren-shava", action='store_true', help="make silukin table acc. keren shava (default is spizer)")

args=parser.parse_args()
is_spizer = not args.keren_shava

c = ribit_kvua_zmuda_monthly_return(args.principal, args.years, args.rate, args.madad, is_spizer)

total = c * args.years * 12
print("monthly return is %f" % c)
print("total return is %f" % total)
print("shekel lemashkanta %f" % (total / args.principal))
print("============")

