אמל"ק - אופציות או מניות לעובדים, מי מה מו למה (RSU, espp, אופציות לסטרטאפ, סעיף 102 וכל הג'אז) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
נינגו'תתתתתתתתתתתתתתתתתתתת ברוך השם אני נושם וגם זה לא מובן מאליו בכלל עם הקורונה. 
חשבתם פעם למה בכלל יש עניין עם קבלת מניות או אופציות מהעבודה? אם לא חשבתם, אז נכשלתי בעבודה שלי. אם אתם נינג'ות ואתם איתנו כבר כמה זמן, אתם צריכים לדעת שכל שקל שאתם מקבלים מהמעסיק שלכם עולה לו שתיים, כלומר בעולם מושלם ללא מסים הייתם מקבלים את שני השקלים. 
זה תקף גם על האוטו, הסיבוס, הטרקטורונים בקפריסין בנופש חברה, הלבן היקר וזריקת הגמדים למטרה בקריסמס וכיו"ב. כל דבר שהמעסיק נותן לכם - עולה לו פי שתיים. חוץ מכמה יוצאי דופן חריגים:
תנאים סוציאליים - אם עדיין לא קראתם את מדריך הפנסיה אז חראם על המאות שקלים לחודש שאתם מורידים בשירותים (לינק בתגובה הראשונה) 
מניות ואופציות של החברה שאתם עובדים בה - על זה בדיוק אנחנו מדברים.
אם לא קיבלתם אף פעם אופציות או מניות מהמעסיק אז תבינו מתי תתחילו לקבל, זה חשוב. ואם לא נראה לכם שתקבלו אז כנראה שאתם לא בהי-טק, ואז העצה שלי היא שתעברו להיטק. 
מילה על התהליך היצירתי בנינג'ה, שתבינו מאיפה הגיע הפוסט הזה. כל מי שנמצא בתוכנית הליווי שלנו לומד על הנושא הזה, ורק ביום שבת בבוקר בברנץ' גיליתי שידידה שלי עומדת לזרוק לפח פטור ממס ענק על מניות של גוגל. רק כי לא היה לה מושג איך עובד הנושא הזה. ואיך יהיה לה, היא לא בתוכנית הליווי שלנו. הבנתי שאין שום סיכוי שהיא הייתה יודעת את זה אם היא לא הייתה מדברת איתי, ולכן חשוב שאני אדבר על זה איתכם.
מפה לשם, החלטתי גם לכתוב את הפוסט, וגם להזמין את כולכם לוובינר שאני מעביר למשתתפי תוכנית הליווי שלנו (אפשר גם לא להגיע ולקבל הקלטה) - לינק בתגובה הראשונה. 
***
דיסקליימר - אני כותב פה פוסט לאנשים בטווח רחב של הבנה, ולכן מטבע הדברים אל תקחו את זה לבנק, תבינו את הקונספט, ואם יש לכם החלטה אופרטיבית לעשות דברו עם הנאמן או רו"ח או אפילו איתי, אבל בחייאת, אל תנטפקו לי את הפוסט רק כי לא התייחסתי לתיקון תיקון מס' 132 תשס"ג-2002.
***
אז מה זה בכלל? 
זאת דרך לתת לעובד הטבה (=שכר) שמצד אחד קושרת את הצלחתו הכלכלית עם הצלחת החברה, ומצד שני דוחה את המס שהוא צריך לשלם על ההטבה הזאת. לדוגמה - שירן עובדת בחברת גוגל. עם כניסתה לחברה קיבלה אקוויטי ( = מניות) בשווי $100K לארבע שנים. כל שנה היא מקבלת $25K, או כל חודש אלפיים דולר בקירוב של הקירובים. 
אם היא הייתה מקבלת מניות סתם ככה, הייתה צריכה לשלם 1000 דולר במס על המניות, כל חודש. 
בגלל שהיא בתוכנית הRSU (או אופציות בסטרטאפ, לא חשוב כרגע), היא לא צריכה לשלם את המס. 
🍗 מתי שירן תשלם את המס?
מתי שהיא תמכור את המניות.
כמו לדאבל-שווארמה לארוחת צהריים, וכמו לכל דבר בחיים, גם לתוכניות דחיית מס יש יתרונות וחסרונות. נבין למה זה כדאי ולמה לא, וכל אחד צריך לבדוק את השיקולים שלו
🍗 למה זה כדאי? 
כי אם דוחים מס בתכלס משלמים פחות. 
נעשה דוגמה מספרית. שירן קיבלת את אותם מאה אלף דולר בתחילת 2020, כלומר 50 אלף דולר אחרי מס הכנסה.
 היא קנתה איתם מניות של גוגל (לא שופט, יש אנשים שעושים דברים יותר גרועים עם הכסף). 
אחרי עשר שנים נניח ששווי המנייה הכפיל את עצמו, היא מכרה, שילמה מיסים, ונותרו לה בחשבון טיפה פחות מ90 אלף דולר. 
אם היא הייתה עושה את אותה עסקה בדיוק, אבל עם האקוויטי גוגל שהיא קיבלה, היא לא הייתה משלמת את המס כשהיא קיבלה את המניות, אלא רק ביום המכירה. ואז היו לה בחשבון 125 אלף דולר. או 40% יותר. 
בנוסף, יש עוד יתרון נדיר-אך-מגניב, אם שירן תמצא שנה עתידית שבה היא לא עובדת, ודווקא בשנה הזאת היא תמכור את המניות, היא גם לא תשלם מס הכנסה! היוש פרישה מוקדמת במימון משלם המסים, היוש טיול שנה באסיה עם הילדים בקראוון והיוש לא לשלם מסים
🍗 למה זה לא כדאי? 
כי רבאק כמה מניות גוגל יכול להחזיק בנאדם אחד סביר?! כבר שנים ארוכות שחזית המחקר מראה שעדיף להשקיע במדד ולא במניה בודדת. אני מכיר היטקיסטים שחצי מההון שלהם ואף יותר יושב בחברה אחת בגלל התוכניות הללו
🍗 אז מה עושים?
1. לא עושים כלום בלי ללמוד ולהבין ולדעת, אתם לא חיות
2. מבינים שזה חלק עצום מהשכר שלכם, וזוכרים את זה למו"מ הבא
3. מאזנים בין הסיבות למה זה כדאי ולמה לא, בהתאם לצרכים האישיים של המשפחה האישית שלכם
איך לומדים?
1. האופציה המועדפת עליי היא שתבואו לוובינר שלי https://secure.cardcom.solutions/e/xHG9
2. מי שמעדיף יכול ללמוד מהלינק הבא: https://www.nevo.co.il/law_html/Law01/255_001.htm#Seif173
3. אפשר לחפש בפורום הסולידית
4. מוזמנים להוסיף בתגובות
