#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt

#calculator equivalent to this one: https://www.bankrate.com/banking/savings/simple-savings-calculator/
def annual_rate_to_month_rate(rate_y):
    return ((rate_y + 1) ** (1/12)) - 1

#S = float(sys.argv[1])
#y = int(sys.argv[2])
#m = float(sys.argv[3])
#r = annual_rate_to_month_rate(float(sys.argv[4])/100)
#amlat_knia = float(sys.argv[5]) / 100
#tax = float(sys.argv[6]) / 100
#dmei_nihul = annual_rate_to_month_rate(float(sys.argv[7])/100)

#TODO - use begin_month
def total_sum_over_years(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month):
    monthly = monthly - (amla * monthly)

    total = init_dep
    totals = np.arange(0) #empty
    for a in range(12 * years[0]):
        total = total + (monthly_rate * total) + monthly - (dmei_nihul * total)
    totals = np.append(totals, total)
    for a in range(np.size(years) - 1):
        for b in range(12):
            total = total + (monthly_rate * total) + monthly - (dmei_nihul * total)
        totals = np.append(totals, total)

    total = total - (tax * total)
    print("final sum: %s %r %r" % (total,monthly_rate,monthly))

def total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month):
    monthly = monthly - (amla * monthly)

    total = init_dep
    for a in range(12 * years):
        if begin_month:
            total = total + monthly
        total = total + (monthly_rate * total)
        total = total - (dmei_nihul * total)
        if not begin_month:
            total = total + monthly
#        print(total)

    total = total - (tax * total)
#    print("final sum: %s %r %r" % (total,monthly_rate,monthly))
    return total

def total_sum_monthly_from_file(init_dep, years, monthly_file, monthly_rate, amla, dmei_nihul, tax, begin_month):
    total = init_dep
    with open(monthly_file) as f:
        for line in f:
            monthly = float(line.rstrip())

            monthly = monthly - (amla * monthly)

            if begin_month:
                total = total + monthly
            total = total + (monthly_rate * total)
            total = total - (dmei_nihul * total)
            if not begin_month:
                total = total + monthly
    #        print(total)

    total = total - (tax * total)
#    print("final sum: %s %r %r" % (total,monthly_rate,monthly))
    return total

parser = argparse.ArgumentParser("this is a an extentsion to the calculator https://www.bankrate.com/banking/savings/simple-savings-calculator/")

parser.add_argument("-s", "--initial-deposit",type=float, required=True)
parser.add_argument("-y", "--years",type=int, help="over a perions of ..", required=True)
parser.add_argument("-r", "--apy", type=float, help="yearly rate APY (%%)", required=True)
parser.add_argument("-m", "--monthly-contrib", default = 0, help="monthly contributions - a number or a files with number in each row", required=True)
parser.add_argument("-t", "--tax", type=float, help="the taxes  מס רווחי הון (%%)", required=True)
parser.add_argument("-a", "--amlat-knia", type=float, help="עמלת קניה  (%%)", required=True)
parser.add_argument("-d", "--dmei-nihul", type=float, help="דמי נהו ל שנתיים  (%%)", required=True)
parser.add_argument("-b", "--contrib-begin-of-month", action='store_true', help="monthly contrib at start of month (without that option monthly contrib is at end of month", required = False)
parser.add_argument("-x", "--changing-arg",type=str, choices = ['s', 'y', 'r', 'm', 'a', 'd'], help="chaning arguments for graph", required=False)

args=parser.parse_args()

init_dep = args.initial_deposit
years = args.years
monthly = args.monthly_contrib

monthly_rate = annual_rate_to_month_rate(args.apy / 100)
amla = args.amlat_knia / 100
dmei_nihul = annual_rate_to_month_rate(args.dmei_nihul / 100)
tax = args.tax / 100

begin_month = args.contrib_begin_of_month

try:
    monthly = float(monthly)
except:
    total = total_sum_monthly_from_file(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    print(total)
    print("{:,}".format(total))
    sys.exit()

c = args.changing_arg
if c is None:
    total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    print(total)
elif c == 's':
        init_dep = np.arange(10000,100000,10000)
        total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
        plt.plot(init_dep, total)
elif c == 'y':
    years = np.arange(10,30,1)
    total = total_sum_over_years(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    plt.plot(years, total)
elif c == 'r':
    m = annual_rate_to_month_rate(2 / 100)
    M = annual_rate_to_month_rate(6 / 100)
    j = annual_rate_to_month_rate(0.1 / 100)
    monthly_rate = np.arange(m, M, j)
    print(monthly_rate)
    total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    print(total)
    plt.plot(np.delete(np.arange(2,6,0.1), -1), total)

elif c == 'm':
    monthly = np.arange(100,10000,100)
    total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    plt.plot(monthly, total)
elif c == 'a':
    m = annual_rate_to_month_rate(0.05 / 100)
    M = annual_rate_to_month_rate(1.5 / 100)
    j = annual_rate_to_month_rate(0.01 / 100)
    amla = np.arange(m, M, j)
    total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    plt.plot(np.delete(np.arange(0.05,1.5,0.01), -1), total)
elif c == 'd':
    m = annual_rate_to_month_rate(0.05 / 100)
    M = annual_rate_to_month_rate(1.5 / 100)
    j = annual_rate_to_month_rate(0.01 / 100)
    dmei_nihul = np.arange(m, M, j)
    total = total_sum(init_dep, years, monthly, monthly_rate, amla, dmei_nihul, tax, begin_month)
    plt.plot(np.delete(np.arange(0.05,1.5,0.01), -1), total)

plt.show()
